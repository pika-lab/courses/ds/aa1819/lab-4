package it.unibo.sd1819.lab4.ts.logic;

import java.util.Objects;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;
import it.unibo.sd1819.lab4.core.Template;
import it.unibo.sd1819.lab4.core.Tuple;

public class LogicTemplate implements Template {

	@Override
	public boolean matches(final Tuple tuple) {
		if (tuple instanceof LogicTuple) {
			return this.asTupleTerm().match(((LogicTuple) tuple).getTerm());
		}

		return false;
	}

	private final Struct term;
	
	public LogicTemplate(final String term) {
		this(Term.createTerm(Objects.requireNonNull(term)));
	}
	
	public LogicTemplate(final Term term) {
		Objects.requireNonNull(term);
		if (term instanceof Struct && ((Struct) term).getName().equals("template") && ((Struct) term).getArity() == 1) {
			this.term = (Struct) term;
		} else {
			this.term = new Struct("template", term);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((term == null) ? 0 : term.toString().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogicTemplate other = (LogicTemplate) obj;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.toString().equals(other.term.toString()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return term.toString();
	}
	
	public Struct getTerm() {
		return term;
	}
	
	public Struct asTupleTerm() {
		return new Struct("tuple", term.getArg(0));
	}

	public static Struct pattern() {
		return new Struct("template", new Var("TT"));
	}
}
