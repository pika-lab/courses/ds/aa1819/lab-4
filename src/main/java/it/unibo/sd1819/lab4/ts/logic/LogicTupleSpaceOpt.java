package it.unibo.sd1819.lab4.ts.logic;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import it.unibo.sd1819.lab4.UnionOf;
import it.unibo.sd1819.lab4.core.NegatedTupleSpace;

public class LogicTupleSpaceOpt extends LogicTupleSpace implements NegatedTupleSpace<LogicTuple, LogicTemplate> {

	public LogicTupleSpaceOpt(ExecutorService executor) {
		super(executor);
	}
	

	public LogicTupleSpaceOpt(String name, ExecutorService executor) {
		super(name, executor);
	}
	
	public Future<LogicTemplate> absent(String template) {
		return absent(new LogicTemplate(template));
	}
	
	public Future<LogicTemplate> absent(LogicTuple tuple) {
		return absent(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<UnionOf<LogicTemplate, LogicTuple>> tryAbsent(String template) {
		return tryAbsent(new LogicTemplate(template));
	}
	
	public Future<UnionOf<LogicTemplate, LogicTuple>> tryAbsent(LogicTuple tuple) {
		return tryAbsent(new LogicTemplate(tuple.asTemplateTerm()));
	}

	@Override
	public Future<LogicTemplate> absent(LogicTemplate template) {
		log("Requested `absent` operation for: %s", template);
        final CompletableFuture<LogicTemplate> promise = new CompletableFuture<>();
        getExecutor().execute(() -> this.handleAbsent(template, promise));
        return promise;
	}

	// TODO implement
	private void handleAbsent(LogicTemplate template, CompletableFuture<LogicTemplate> promise) {
		throw new IllegalStateException("Not implemented");
	}


	@Override
	public Future<UnionOf<LogicTemplate, LogicTuple>> tryAbsent(LogicTemplate template) {
		log("Requested `tryAbsent` operation for: %s", template);
        final CompletableFuture<UnionOf<LogicTemplate, LogicTuple>> promise = new CompletableFuture<>();
        getExecutor().execute(() -> this.handleTryAbsent(template, promise));
        return promise;
	}

	// TODO implement
	private void handleTryAbsent(LogicTemplate template, CompletableFuture<UnionOf<LogicTemplate, LogicTuple>> promise) {
		throw new IllegalStateException("Not implemented");
	}

}
