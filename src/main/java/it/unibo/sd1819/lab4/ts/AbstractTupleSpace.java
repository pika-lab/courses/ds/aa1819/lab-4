package it.unibo.sd1819.lab4.ts;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.collections4.MultiSet;

import it.unibo.sd1819.lab4.core.ExtendedTupleSpace;
import it.unibo.sd1819.lab4.core.Template;
import it.unibo.sd1819.lab4.core.Tuple;

public abstract class AbstractTupleSpace<T extends Tuple, TT extends Template> implements ExtendedTupleSpace<T, TT> {
    private final ExecutorService executor;
    private final ReentrantLock lock = new ReentrantLock(true);
    private final String name;
    
    public AbstractTupleSpace(final String name, final ExecutorService executor) {
        this.name = Objects.requireNonNull(name) + "#" + System.identityHashCode(this);
        this.executor = Objects.requireNonNull(executor);
    }
    
    protected ReentrantLock getLock() {
    	return lock;
    }
    
    protected ExecutorService getExecutor() {
		return executor;
	}
    
    protected void log(String format, Object... args) {
        System.out.printf("[" + getName() + "] " + format + "\n", args);
    }

    public String getName() {
        return name;
    }
    
    @Override
    public Future<T> read(final TT template) {
        log("Requested `read` operation on template: %s", template);
        final CompletableFuture<T> result = new CompletableFuture<>();
        executor.execute(() -> this.handleRead(template, result));
        return result;
    }
    
    protected abstract void handleRead(TT template, CompletableFuture<T> result);

	@Override
    public Future<T> take(final TT template) {
        log("Requested `take` operation on template: %s", template);
        final CompletableFuture<T> result = new CompletableFuture<>();
        executor.execute(() -> this.handleTake(template, result));
        return result;
    }
    
	protected abstract void handleTake(TT template, CompletableFuture<T> result);

	@Override
    public Future<T> write(final T tuple) {
        log("Requested `write` operation for tuple: %s", tuple);
        final CompletableFuture<T> result = new CompletableFuture<>();
        executor.execute(() -> this.handleWrite(tuple, result));
        return result;
    }
    
	protected abstract void handleWrite(T tuple, CompletableFuture<T> result);

	@Override
    public Future<MultiSet<? extends T>> get() {
        log("Requested `get` operation");
        final CompletableFuture<MultiSet<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleGet(result));
        return result;
    }
    
	protected abstract void handleGet(CompletableFuture<MultiSet<? extends T>> result);

	@Override
    public Future<Integer> getSize() {
        log("Requested `getSize` operation");
        final CompletableFuture<Integer> result = new CompletableFuture<>();
        executor.execute(() -> this.handleGetSize(result));
        return result;
    } 
    
	protected abstract void handleGetSize(CompletableFuture<Integer> result);
	
	@Override
	public Future<MultiSet<? extends T>> readAll(TT template) {
		log("Requested `readAll` operation on template %s", template);
        final CompletableFuture<MultiSet<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleReadAll(template, result));
        return result;
	}

	protected abstract void handleReadAll(TT template, CompletableFuture<MultiSet<? extends T>> result);

	@Override
	public Future<MultiSet<? extends T>> takeAll(TT template) {
		log("Requested `takeAll` operation on template %s", template);
        final CompletableFuture<MultiSet<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleTakeAll(template, result));
        return result;
	}
	
	protected abstract void handleTakeAll(TT template, CompletableFuture<MultiSet<? extends T>> result);

	@Override
	public Future<MultiSet<? extends T>> writeAll(Collection<? extends T> tuples) {
		log("Requested `writeAll` operation on tuples: %s", tuples);
        final CompletableFuture<MultiSet<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleWriteAll(tuples, result));
        return result;
	}

	protected abstract void handleWriteAll(Collection<? extends T> tuples, CompletableFuture<MultiSet<? extends T>> result);

	@Override
	public Future<Optional<T>> tryTake(TT template) {
		log("Requested `tryTake` operation on template: %s", template);
        final CompletableFuture<Optional<T>> result = new CompletableFuture<>();
        executor.execute(() -> this.tryTake(template, result));
        return result;
	}
	
	protected abstract void tryTake(TT template, CompletableFuture<Optional<T>> result);

	@Override
	public Future<Optional<T>> tryRead(TT template) {
		log("Requested `tryRead` operation on template: %s", template);
        final CompletableFuture<Optional<T>> result = new CompletableFuture<>();
        executor.execute(() -> this.tryRead(template, result));
        return result;
	}
	
	protected abstract void tryRead(TT template, CompletableFuture<Optional<T>> result);

	@Override
	public String toString() {
		return "TextTupleSpace [name=" + name + "]";
	}
    
    protected enum RequestTypes {
        READ, TAKE, ABSENT;
    }

	protected class PendingRequest {
    	private final RequestTypes requestType;
    	private final TT template;
    	private final CompletableFuture<T> promise;
    	
		public PendingRequest(RequestTypes requestType, TT template, CompletableFuture<T> promise) {
			this.requestType = Objects.requireNonNull(requestType);
			this.template = Objects.requireNonNull(template);
			this.promise = Objects.requireNonNull(promise);
		}

		public RequestTypes getRequestType() {
			return requestType;
		}

		public TT getTemplate() {
			return template;
		}

		public CompletableFuture<T> getPromise() {
			return promise;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((promise == null) ? 0 : promise.hashCode());
			result = prime * result + ((requestType == null) ? 0 : requestType.hashCode());
			result = prime * result + ((template == null) ? 0 : template.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PendingRequest other = (PendingRequest) obj;
			if (promise == null) {
				if (other.promise != null)
					return false;
			} else if (!promise.equals(other.promise))
				return false;
			if (requestType != other.requestType)
				return false;
			if (template == null) {
				if (other.template != null)
					return false;
			} else if (!template.equals(other.template))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "PendingRequest [requestType=" + requestType + ", template=" + template + ", promise=" + promise + "]";
		}
    	
    }
}
