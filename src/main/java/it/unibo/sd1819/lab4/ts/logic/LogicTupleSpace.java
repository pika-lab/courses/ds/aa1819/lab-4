package it.unibo.sd1819.lab4.ts.logic;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;

import alice.tuprolog.NoMoreSolutionException;
import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import it.unibo.sd1819.lab4.core.ExtendedTupleSpace;
import it.unibo.sd1819.lab4.ts.AbstractTupleSpace;

public class LogicTupleSpace extends AbstractTupleSpace<LogicTuple, LogicTemplate> implements ExtendedTupleSpace<LogicTuple, LogicTemplate> {
	
	private Prolog engine = new Prolog();
	private MultiSet<PendingRequest> pendingRequests = new HashMultiSet<>();

	public LogicTupleSpace(String name, ExecutorService executor) {
		super(name, executor);
	}

	public LogicTupleSpace(final ExecutorService executor) {
		this(LogicTupleSpace.class.getSimpleName(), executor);
	}
	
	public Future<LogicTuple> read(String template) {
		return read(new LogicTemplate(template));
	}
	
	public Future<LogicTuple> read(LogicTuple tuple) {
		return read(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<LogicTuple> take(String string) {
		return take(new LogicTemplate(string));
	}
	
	public Future<LogicTuple> take(LogicTuple tuple) {
		return take(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<LogicTuple> write(String string) {
		return write(new LogicTuple(string));
	}
	
	public Future<Optional<LogicTuple>> tryRead(String template) {
		return tryRead(new LogicTemplate(template));
	}
	
	public Future<Optional<LogicTuple>> tryRead(LogicTuple tuple) {
		return tryRead(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<Optional<LogicTuple>> tryTake(String template) {
		return tryTake(new LogicTemplate(template));
	}
	
	public Future<Optional<LogicTuple>> tryTake(LogicTuple tuple) {
		return tryTake(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<MultiSet<? extends LogicTuple>> readAll(String template) {
		return readAll(new LogicTemplate(template));
	}
	
	public Future<MultiSet<? extends LogicTuple>> readAll(LogicTuple tuple) {
		return readAll(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<MultiSet<? extends LogicTuple>> takeAll(String template) {
		return takeAll(new LogicTemplate(template));
	}
	
	public Future<MultiSet<? extends LogicTuple>> takeAll(LogicTuple tuple) {
		return takeAll(new LogicTemplate(tuple.asTemplateTerm()));
	}
	
	public Future<MultiSet<? extends LogicTuple>> writeAll(String tuple1, String... tuples) {
		final List<LogicTuple> ts = Stream.concat(Stream.of(tuple1), Stream.of(tuples))
				.map(LogicTuple::new)
				.collect(Collectors.toList());
		return writeAll(ts);
	}

	@Override
	protected void handleTake(LogicTemplate template, CompletableFuture<LogicTuple> promise) {
		getLock().lock();
		try {
			final SolveInfo info = engine.solve(new Struct("retract", template.asTupleTerm()));
			if (info.isSuccess()) {
				final Struct retract = (Struct) info.getSolution();
				promise.complete(new LogicTuple(retract.getTerm(0)));
			} else {
				pendingRequests.add(new PendingRequest(RequestTypes.TAKE, template, promise));
			}
		} catch (NoSolutionException e) {
			throw new IllegalStateException("This should never happen");
		} finally {
			getLock().unlock();
		}
	}

	@Override
	protected void handleWrite(LogicTuple tuple, CompletableFuture<LogicTuple> promise) {
		getLock().lock();
        try {
            resumePendingRequests(tuple).ifPresent(this::insertTuple);
            promise.complete(tuple);
        } finally {
        	getLock().unlock();
        }
	}
	
	protected Optional<LogicTuple> resumePendingRequests(LogicTuple insertedTuple) {
		Optional<LogicTuple> result = Optional.of(insertedTuple);
        final Iterator<PendingRequest> i = pendingRequests.iterator();
        while (i.hasNext()) {
            final PendingRequest pendingRequest = i.next();
            if (pendingRequest.getTemplate().matches(insertedTuple)) {
            	i.remove();
                if (pendingRequest.getRequestType() == RequestTypes.TAKE) {
                	pendingRequest.getPromise().complete(insertedTuple);
                	result = Optional.empty();
                    break;
                } else if (pendingRequest.getRequestType() == RequestTypes.READ) {
                	pendingRequest.getPromise().complete(insertedTuple);
                }
            }
        }
        return result;
	}
	
	protected boolean insertTuple(LogicTuple tuple) {
		final SolveInfo si = engine.solve(new Struct("assert", tuple.getTerm()));
		engine.solveEnd();
		return si.isSuccess();
	}

	@Override
	protected void handleGet(CompletableFuture<MultiSet<? extends LogicTuple>> promise) {
		getLock().lock();
		final MultiSet<LogicTuple> result = new HashMultiSet<>();
		try {
			for (SolveInfo info = engine.solve(LogicTuple.pattern()); info.isSuccess(); info = engine.solveNext()) {
				result.add(new LogicTuple(info.getSolution()));
			}
			promise.complete(result);
		} catch (NoSolutionException e) {
			throw new IllegalStateException("This should never happen");
		} catch (NoMoreSolutionException e) {
			promise.complete(result);
		} finally {
			getLock().unlock();
		}
	}
	
	@Override // TODO implement
	protected void handleRead(LogicTemplate template, CompletableFuture<LogicTuple> promise) {
		throw new IllegalStateException("Not implemented");
	}

	@Override // TODO implement
	protected void handleGetSize(CompletableFuture<Integer> promise) {
		throw new IllegalStateException("Not implemented");
	}

	@Override // TODO implement
	protected void handleReadAll(LogicTemplate template, CompletableFuture<MultiSet<? extends LogicTuple>> promise) {
		throw new IllegalStateException("Not implemented");
	}

	@Override // TODO implement
	protected void handleTakeAll(LogicTemplate template, CompletableFuture<MultiSet<? extends LogicTuple>> promise) {
		throw new IllegalStateException("Not implemented");
	}

	@Override // TODO implement
	protected void handleWriteAll(Collection<? extends LogicTuple> tuples, CompletableFuture<MultiSet<? extends LogicTuple>> promise) {
		throw new IllegalStateException("Not implemented");
	}

	@Override // TODO implement
	protected void tryTake(LogicTemplate template, CompletableFuture<Optional<LogicTuple>> promise) {
		throw new IllegalStateException("Not implemented");
	}

	@Override // TODO implement
	protected void tryRead(LogicTemplate template, CompletableFuture<Optional<LogicTuple>> promise) {
		throw new IllegalStateException("Not implemented");
	}
	
	protected Prolog getEngine() {
		return engine;
	}
	
	protected MultiSet<PendingRequest> getPendingRequests() {
		return pendingRequests;
	}

}
