package it.unibo.sd1819.lab4.core;

import java.util.Optional;
import java.util.concurrent.Future;

public interface PredicativeTupleSpace<T extends Tuple, TT extends Template> extends TupleSpace<T, TT> {
	Future<Optional<T>> tryTake(TT template);
	Future<Optional<T>> tryRead(TT template);
}
