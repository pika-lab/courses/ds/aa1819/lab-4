package it.unibo.sd1819.lab4.core;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.MultiSet;

public interface BulkTupleSpace<T extends Tuple, TT extends Template> extends TupleSpace<T, TT>  {
	Future<MultiSet<? extends T>> readAll(TT template);
	Future<MultiSet<? extends T>> takeAll(TT template);
	Future<MultiSet<? extends T>> writeAll(Collection<? extends T> tuples);
	
	default Future<MultiSet<? extends T>> writeAll(T tuple1, T... otherTuples) {
		final List<T> tuples = Stream.concat(Stream.of(tuple1), Stream.of(otherTuples)).collect(Collectors.toList());
        return writeAll(tuples);
	}
}
