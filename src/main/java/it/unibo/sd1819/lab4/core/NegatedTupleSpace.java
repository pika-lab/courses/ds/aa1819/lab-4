package it.unibo.sd1819.lab4.core;

import java.util.concurrent.Future;

import it.unibo.sd1819.lab4.UnionOf;
import it.unibo.sd1819.lab4.ts.logic.LogicTemplate;

public interface NegatedTupleSpace<T extends Tuple, TT extends Template> extends TupleSpace<T, TT> {
	Future<LogicTemplate> absent(TT template);
	Future<UnionOf<TT, T>> tryAbsent(TT template);
}
