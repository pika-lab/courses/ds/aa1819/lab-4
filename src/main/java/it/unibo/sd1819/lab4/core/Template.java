package it.unibo.sd1819.lab4.core;

public interface Template {
    boolean matches(Tuple tuple);
}
