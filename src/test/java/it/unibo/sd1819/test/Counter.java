package it.unibo.sd1819.test;

public final class Counter {
    private long value;
    
    public Counter(int value) {
        this.value = value;
    }
    
    public Counter() {
        this(0);
    }
    
    public Counter inc() {
        this.value++;
        return this;
    }
    
    public Counter inc(int i) {
        this.value += i;
        return this;
    }

    @Override
    public String toString() {
        return "Counter [value=" + value + "]";
    }

    public long getValue() {
        return value;
    }
    
}
