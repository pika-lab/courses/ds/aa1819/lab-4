package it.unibo.sd1819.lab4.logic.test;

import alice.tuprolog.Int;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.NoMoreSolutionException;
import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;
import it.unibo.sd1819.lab4.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab4.ts.logic.LogicTuple;
import junit.framework.TestCase;

public class TestLogic extends TestCase {
	
	// check if "person(name(giovanni), surname(ciatto), date([4, 1, 1992]))"
	// matches person(name(N), S, date([D, M, 1992]))
	public void testParsingAndUnification() {	
		final Term t1 = Term.createTerm("person(name(giovanni), surname(ciatto), date([4, 1, 1992]))");
		
		final Term t2 = new Struct("person",
			new Struct("name", new Var("N")),
			new Var("S"),
			new Struct("date", 
				new Struct(new Term[] { 
					new Var("D"), 
					new Var("M"), 
					new Int(1992)  
				})
			)
		);
				
		System.out.println(t1); // person(name(giovanni),surname(ciatto),date([4,1,1992]))
		System.out.println(t2); // person(name(N),S,date([D,M,1992]))
		
		assertTrue(t1.match(t2));
	}
	
	public void testAssertSolveRetract() throws MalformedGoalException, NoSolutionException, NoMoreSolutionException {	
		final Prolog engine = new Prolog();
		
		final Term tuple1 = Term.createTerm("tuple(msg(hello))");
		final Term tuple2 = Term.createTerm("tuple(msg(world))");
		final Term template = Term.createTerm("tuple(msg(M))");
		
		assertFalse(engine.solve(template).isSuccess());
		assertTrue(engine.solve(new Struct("assert", tuple1)).isSuccess());
		assertTrue(engine.solve(String.format("assert(%s).", tuple2)).isSuccess());
		
		SolveInfo si = engine.solve(template);         assertTrue(si.isSuccess());
		assertTrue(si.getSolution().match(tuple1));    
		assertTrue(si.hasOpenAlternatives());
		si = engine.solveNext();                       assertTrue(si.isSuccess());
		assertTrue(si.getSolution().match(tuple2));  
		assertFalse(si.hasOpenAlternatives());
		
		assertTrue(engine.solve(new Struct("retract", template)).isSuccess());
		
		si = engine.solve(template);                   assertTrue(si.isSuccess());
		assertTrue(si.getSolution().match(tuple2));
		assertFalse(si.hasOpenAlternatives());
		
		assertTrue(engine.solve(new Struct("retract", template)).isSuccess());
		
		assertFalse(engine.solve(template).isSuccess());
	}
	
	public void testTupleAndTemplateUsage() throws NoSolutionException {
		final Prolog engine = new Prolog();
		final LogicTuple tuple = new LogicTuple("msg(hello)");
		final LogicTemplate template = new LogicTemplate("msg(Payload)");
		
		// out
		assertTrue(engine.solve(new Struct("assert", tuple.getTerm())).isSuccess());
		
		// rd
		SolveInfo si = engine.solve(template.asTupleTerm());
		assertTrue(si.isSuccess());
		assertTrue(si.getSolution().match(tuple.getTerm()));
		
		// in
		si = engine.solve(new Struct("retract", template.asTupleTerm()));
		assertTrue(si.isSuccess());
		Struct solution = (Struct) si.getSolution();
		assertTrue(solution.getArg(0).match(tuple.getTerm()));
	}

}
