package it.unibo.sd1819.lab4.ts.logic.test;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;
import org.junit.Before;
import org.junit.Test;

import it.unibo.sd1819.lab4.ActiveObject;
import it.unibo.sd1819.lab4.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab4.ts.logic.LogicTuple;
import it.unibo.sd1819.lab4.ts.logic.LogicTupleSpaceOpt;
import it.unibo.sd1819.test.ConcurrentTestHelper;

public class TestLogicTupleSpaceOpt {} // extends TestLogicTupleSpace {
//    
//    private LogicTupleSpaceOpt tupleSpaceOpt;
//    
//    @Before
//    public void setUp() throws Exception {
//        executor = Executors.newSingleThreadExecutor();
//        tupleSpace = tupleSpaceOpt = new LogicTupleSpaceOpt(executor);
//        test = new ConcurrentTestHelper();
//        rand = new Random();
//    }
//
//    
//    @Test
//    public void testAbsentReturns() throws Exception {
//		test.setThreadCount(1);
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertEventuallyReturns(tupleSpaceOpt.absent("f(X)"));
//            	stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//    }
//    
//    @Test
//    public void testAbsentSuspends() throws Exception {
//		test.setThreadCount(1);
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertEventuallyReturns(tupleSpaceOpt.write("f(1)"));
//            	test.assertBlocksIndefinitely(tupleSpaceOpt.absent("f(X)"));
//            	stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//    }
//    
//    @Test
//    public void testTryAbsentSucceeds() throws Exception {
//		test.setThreadCount(1);
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertTrue(tupleSpaceOpt.tryAbsent("f(X)"), union -> union.isLeft());
//            	stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//    }
//    
//    @Test
//    public void testTryAbsentFails() throws Exception {
//		test.setThreadCount(1);
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertEventuallyReturns(tupleSpaceOpt.write("f(1)"));
//            	test.assertTrue(tupleSpaceOpt.tryAbsent("f(X)"), union -> union.isRight());
//            	
//            	stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//    }
//    
//    @Test
//    public void testTakeResumesAbsent() throws Exception {
//    	test.setThreadCount(2);
//    	
//        final LogicTuple tuple = new LogicTuple("foo(bar)");
//        
//        final ActiveObject bob = new ActiveObject("Bob") {
//            
//            @Override 
//            protected void loop() throws Exception {
//                test.assertEquals(tupleSpaceOpt.take("foo(B)"), tuple);
//                stop();
//            }
//
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        };
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertEventuallyReturns(tupleSpaceOpt.write(tuple));
//            	final Future<LogicTemplate> toBeAbsent = tupleSpaceOpt.absent("foo(X)");
//            	bob.start();
//            	test.assertEventuallyReturns(toBeAbsent);
//                stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//        bob.await();
//    }
//    
//    @Test
//    public void testTryTakeResumesAbsent() throws Exception {
//    	test.setThreadCount(2);
//    	
//        final LogicTuple tuple = new LogicTuple("foo(bar)");
//        
//        final ActiveObject bob = new ActiveObject("Bob") {
//            
//            @Override 
//            protected void loop() throws Exception {
//                test.assertTrue(tupleSpaceOpt.tryTake("foo(B)"), opt -> opt.isPresent() && opt.get().equals(tuple));
//                stop();
//            }
//
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        };
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertEventuallyReturns(tupleSpaceOpt.write(tuple));
//            	final Future<LogicTemplate> toBeAbsent = tupleSpaceOpt.absent("foo(X)");
//            	bob.start();
//            	test.assertEventuallyReturns(toBeAbsent);
//                stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//        bob.await();
//    }
//    
//    @Test
//    public void testTakeAllResumesAbsent() throws Exception {
//    	test.setThreadCount(2);
//
//        final MultiSet<LogicTuple> tuples = new HashMultiSet<>(Arrays.asList(
//        		new LogicTuple("foo(bar)"),
//        		new LogicTuple("foo(baz)")
//    		));
//        
//        final ActiveObject bob = new ActiveObject("Bob") {
//            
//            @Override 
//            protected void loop() throws Exception {
//                test.assertEquals(tupleSpaceOpt.takeAll("foo(B)"), tuples);
//                stop();
//            }
//
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        };
//        
//        final ActiveObject alice = new ActiveObject("Alice") {
//            
//            @Override 
//            protected void loop() throws Exception {
//            	test.assertEquals(tupleSpaceOpt.writeAll(tuples), tuples);
//            	final Future<LogicTemplate> toBeAbsent = tupleSpaceOpt.absent("foo(X)");
//            	bob.start();
//            	test.assertEventuallyReturns(toBeAbsent);
//                stop();
//            }
//            
//            @Override 
//            protected void onEnd() {
//                test.done();
//            }
// 
//        }.start();
//        
//        test.await();
//        alice.await();
//        bob.await();
//    }
//    
//}
